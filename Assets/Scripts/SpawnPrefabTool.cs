﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpawnPrefabTool : MonoBehaviour
{
    public GameObject PrefabToSpawn;

    public List<GameObject> PrefabsList;

    [SerializeField]
    private Raycaster raycaster;
    private Vector3 _positionToPlace;
    private GameObject InstantiatedPrefab;
    private List<PrefabData> placedPrefabs= new List<PrefabData>();
    [SerializeField]
    private LevelEditorData editorData;


    private void OnDestroy()
    {
        editorData.editedGO.AddRange(placedPrefabs);
        AssetDatabase.SaveAssets();
    }

    public Vector3 PositionToPlace
    {
        set
        {
            _positionToPlace = value;
            if (InstantiatedPrefab)
            {
                var positionWithHeihght = _positionToPlace;
                //positionWithHeihght.y += InstantiatedPrefab.GetComponent<Collider>().bounds.extents.y;
                InstantiatedPrefab.transform.position = positionWithHeihght;
            }
        }
    }

    public void CreateInstance()
    {
        if (InstantiatedPrefab != null)
        {
            PlaceObjectAndSave();
        }

        InstantiatedPrefab = Instantiate(PrefabToSpawn, _positionToPlace, Quaternion.Euler(-90,0,0));
    }

    private void PlaceObjectAndSave()
    {
        SetFinalSettings(InstantiatedPrefab);
        placedPrefabs.Add(new PrefabData { position = InstantiatedPrefab.transform.position, name = InstantiatedPrefab.name});
        Debug.Log(placedPrefabs[placedPrefabs.Count - 1].name);
        InstantiatedPrefab = null;
    }

    private void SetFinalSettings(GameObject prefabObject)
    {
        prefabObject.layer = 0;
        prefabObject.GetComponent<Collider>().isTrigger = false;
    }

    [ContextMenu("LoadData")]
    public void LoadPrefabs()
    {
        foreach(PrefabData prefab in editorData.editedGO)
        {
           GameObject go = Instantiate(FindPrefabWithName(prefab.name), prefab.position, FindPrefabWithName(prefab.name).transform.rotation);
           SetFinalSettings(go);
        }
    }

    private GameObject FindPrefabWithName(string name)
    {
        name = name.Replace("(Clone)", "");
        return PrefabsList.Find(x => x.name.Equals(name));
    }

    private void Update()
    {
        PositionToPlace = raycaster.HitPoint;

        if (Input.GetMouseButtonDown(0))
        {
            CreateInstance();
        }

        ScaleUpDown();

        if (Input.GetMouseButtonDown(1))
        {
            ChangeToNextPrefab();
        }
    }

    private void ScaleUpDown()
    {
        if (InstantiatedPrefab != null)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
            {
                InstantiatedPrefab.transform.localScale *= 1.2f;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
            {
                InstantiatedPrefab.transform.localScale *= 0.8f;
            }
        }
    }

    private void ChangeToNextPrefab()
    {

        int i = PrefabsList.FindIndex(x => x.Equals(PrefabToSpawn));
        i++;
        var nextPrefab = PrefabsList[i > PrefabsList.Count ? 0 : i % PrefabsList.Count];
        Debug.Log(nextPrefab.name);
        PrefabToSpawn = nextPrefab;
        Destroy(InstantiatedPrefab);
        InstantiatedPrefab = null;
        CreateInstance();
    }
}
