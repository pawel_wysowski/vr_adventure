﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightVisuals : MonoBehaviour
{
    public Material NightSkybox;
    public Material DaySkybox;

    public GameObject Moonlight;
    public GameObject Sunlight;

    public float DayTime;

    private bool timeOfDay = true;
    public bool TimeOfDay
    {
        get { return timeOfDay; }

        set
        {
            if (timeOfDay != value)
            {
                timeOfDay = value;
                ChangeLight();
            }
        }
    }


    private void Start()
    {
        StartDay();
    }

    private void StartDay()
    {
        transform.DORotate(new Vector3(350, 0, 0), DayTime).SetLoops(-1);
    }

    private void StartNight()
    {
        transform.DORotate(new Vector3(0, 0, 0), DayTime).OnComplete(new TweenCallback(() =>
        {
            ChangeLight();
            StartDay();
        }));
    }

    private void ChangeLight()
    {
        if (Moonlight.activeInHierarchy)
        {
            Sunlight.SetActive(true);
            Moonlight.SetActive(false);
        }
        else
        {
            Sunlight.SetActive(false);
            Moonlight.SetActive(true);
        }
    }
}
