﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChunkManager : MonoBehaviour
{
    public List<Transform> Nodes;
    public List<GameObject> Chunks;

    public Transform Player;

    private Dictionary<Transform, GameObject> nodesWithChunks = new Dictionary<Transform, GameObject>();

    [SerializeField]
    private float distanceToActivate;
    [SerializeField]
    private float distanceToDeactivate;

    private float angleToDetect;

    private GameObject ChunkEnabledCurrently = null;

    private void Start()
    {
        PopulateDictionary();
        angleToDetect = 95;
    }

    private void Update()
    {
        CalculateAndEnableSecond();
    }

    private void PopulateDictionary()
    {
        int i = 0;
        foreach (GameObject chunk in Chunks)
        {
            nodesWithChunks.Add(Nodes[i], chunk);
            i++;
        }
    }

    private void CalculateAndEnableSecond()
    {
        foreach(Transform node in Nodes)
        {
            GameObject chunk;
            nodesWithChunks.TryGetValue(node, out chunk);

            Vector3 directionToTarget = Player.position - node.position;
            float distance = directionToTarget.magnitude;
            if (distance < distanceToActivate)
            {
                chunk.gameObject.SetActive(true);
            }
            else if (distance > distanceToDeactivate)
            {
                chunk.gameObject.SetActive(false);
            }
        }
    }
}
