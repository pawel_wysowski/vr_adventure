﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    private RaycastHit hit;
    public Vector3 HitPoint { get; private set; }
    public LayerMask layerToDetect;
    void Update()
    {
        if (Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0)), out hit, 100, layerToDetect))
        {
            HitPoint = hit.point;
        }
    }
}
