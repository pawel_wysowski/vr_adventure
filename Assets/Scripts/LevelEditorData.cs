﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LvlEditor", menuName = "Data", order = 0)]
public class LevelEditorData : ScriptableObject
{
    public List<PrefabData> editedGO;
}
[System.Serializable]
public class PrefabData
{
    public string name;
    public Vector3 position;
}